#### Torres Trujillo Luis Octavio
##### Matricula: 20201023
##### Tarea 1: Poema
*Los robots son inorgánicos,*  
*No son como los humanos,*  
*Pero aun así son más inteligentes,*  
*Como si su mentalidad fuera de otro mundo.*

*Con una programación avanzada*  
*Y con una mecánica sin igual*  
*Son capaces de hacer grandes cosas*  
*Que nosotros jamás podríamos lograr.*  

![](https://image.freepik.com/vector-gratis/robot-retro-dibujos-animados_10308-279.jpg)